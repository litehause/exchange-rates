<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
//        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
//            'basePath'  => 'path/to/some/directory',
//            'namespace' => 'Application_Model_',
//        ));
    }

    public function indexAction()
    {
        // action body
        $currencies = new Application_Model_DbTable_Currencies();
        $this->view->entries = $currencies->fetchAll();

    }


}

