<?php

class RestController extends Zend_Controller_Action
{
    private $CURRENT_EXCHENGE_RATES_URL = "http://www.cbr.ru/scripts/XML_daily.asp";


    private $cache = null;

    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function loadAction()
    {
        $currencyDAO = new Application_Model_DbTable_Currencies();
        $currencies = $currencyDAO->fetchAll();

        $currencyCodeList = array();
        foreach($currencies as $currency) {
            $currencyCodeList[] = $currency->code;
        }
        $this->getResponse()->setBody(json_encode($this->loadCurrencies($currencyCodeList)));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function addcurrencyAction() {
        $currencyCode = $this->_getParam('currencyCode');
        $currencyDAO = new Application_Model_DbTable_Currencies();
        $curency = $currencyDAO->getCurrencyByCode($currencyCode);
        if (!empty($curency)) {
            $this->getResponse()->setBody('Данная валюта уже есть в базе');
            $this->getResponse()->setHttpResponseCode(400);
            return;
        }
        $currencyValue = $this->loadCurrencies(array($currencyCode))[$currencyCode];
        if (empty($currencyValue)) {
            $this->getResponse()->setBody('К сожалению данная валюта не поддерживается');
            $this->getResponse()->setHttpResponseCode(400);
            return;
        }
        $currencyName = $currencyValue['name'];
        $currencyDAO->addCurrency($currencyCode, $currencyName);
        $result = array();
        $result['code'] = $currencyCode;
        $result['name'] = $currencyName;
        $result['price'] = $currencyValue['price'];

        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function updateAction() {
        $currencyCode = $this->_getParam('currencyCode');
        $value = $this->loadFromServer(array($currencyCode))[$currencyCode];
        if (!$value) {
            $this->getResponse()->setBody('Произошла ошибка обновления');
            $this->getResponse()->setHttpResponseCode(500);
            return;
        }
        $result = array();
        $result['code'] = $currencyCode;
        $result['name'] = $value['name'];
        $result['price'] = $value['price'];
        $this->updateInCache($currencyCode, $value);
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    /**************************************/
    //В сервис даты работы с кэшем
    /**************************************/

    /**
     * Возращает обьект Cache
     * @return null|Zend_Cache_Core|Zend_Cache_Frontend
     */
    private function getCache() {
        if (!$this->cache) {
            $frontendOptions = array(
                'lifetime' => 60 * 60 * 24,                   // время жизни кэша - 24 часа
                'automatic_serialization' => true
            );

            $backendOptions = array('cache_dir' => '/var/shara/');

            $this->cache = Zend_Cache::factory('Output',
                'File',
                $frontendOptions,
                $backendOptions);
        }
        return $this->cache;
    }

    private function getInCache($key) {
        return $this->getCache()->load($key);
    }

    private function saveInCache($key, $value) {
        $this->getCache()->save($value, $key);
    }

    private function updateInCache($key, $value) {
        $this->getCache()->remove($key);
        $this->saveInCache($key, $value);
    }

    /**************************************/
    //В сервис даты работы с загрузкой данных
    /**************************************/

    private function loadCurrencies($currenciesCode) {
        $loadCurrenciesFromService = array();
        $result = array();
        foreach($currenciesCode as $code) {
            $value = $this->getInCache($code);
            if (!$value) {
                $loadCurrenciesFromService[] = $code;
            } else {
                $result[$code] = $value;
            }
        }

        $currencyFromService = $this->loadFromServer($loadCurrenciesFromService);
        foreach($loadCurrenciesFromService as $code) {
            $value = $currencyFromService[$code];
            $result[$code] = $value;
            $this->saveInCache($code, $value);
        }
        return $result;
    }

    private function loadFromServer($currencies) {
        $currentExchangeRates = $this->loadCurrentExchangeRates($this->CURRENT_EXCHENGE_RATES_URL);
        return $this->parseXml($currentExchangeRates['content'], $currencies);
    }

    private function parseXml($xml, $curenciesCode) {
        $xml = simplexml_load_string($xml);
        $result = array();
        foreach($curenciesCode as $code) {
            $valute = $this->findInXml($code, $xml->Valute);
            if ($valute != null) {
                $result[$code] = $valute;
            }
        }
        return $result;
    }

    private function findInXml($code, $xmlValutes) {
        foreach ($xmlValutes as $valute) {
            if ($valute->CharCode == $code) {
                $result = array();
                $value = (int) $valute->Value;
                $nominal = (int) $valute->Nominal;
                $result['price'] = $value / $nominal;
                $result['name'] = (string) $valute->Name;
                return $result;
            }
        }
        return null;
    }

    private function loadCurrentExchangeRates($url)
    {
        $options = array(
            CURLOPT_CUSTOMREQUEST => "GET", //set request type post or get
            CURLOPT_POST => false, //set to GET
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 20, // timeout on connect
            CURLOPT_TIMEOUT => 20, // timeout on response
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);
        $result['content'] = $content;
        $result['code'] = $header['http_code'];
        return $result;
    }


}

