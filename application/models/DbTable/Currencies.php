<?php

class Application_Model_DbTable_Currencies extends Zend_Db_Table_Abstract
{

    protected $_name = 'currencies';


    /**
     * Возврат валюты по ее коду
     * @param $code
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getCurrencyByCode($code)
    {
        return $this->fetchRow($this->select()->where('code=?', $code));
    }

    public function addCurrency($code, $name)
    {
        $data = array(
            'code' => $code,
            'name' => $name,
        );
        $this->insert($data);
    }

}

