var ExchangeRates,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

ExchangeRates = (function() {
  var ADD_CURRENCY_URL, LOAD_PRICE_URL, UPDATE_URL;

  ADD_CURRENCY_URL = '/rest/addcurrency';

  LOAD_PRICE_URL = '/rest/load';

  UPDATE_URL = '/rest/update';

  function ExchangeRates() {
    this.addCurrency = __bind(this.addCurrency, this);
    this.updateCurrencyCode = __bind(this.updateCurrencyCode, this);
    this.generateCurrencyItem = __bind(this.generateCurrencyItem, this);
    this.addCurrencyItems = __bind(this.addCurrencyItems, this);
    this.init();
  }

  ExchangeRates.prototype.init = function() {
    return $.ajax({
      method: 'POST',
      url: LOAD_PRICE_URL,
      success: (function(_this) {
        return function(response) {
          _this.addCurrencyItems(JSON.parse(response));
          return _this.initFormAddCurrency();
        };
      })(this),
      error: function() {
        return alert("Сервис временно не доступен");
      }
    });
  };

  ExchangeRates.prototype.addCurrencyItems = function(currencies) {
    var container;
    container = $('#currencyList');
    container.empty();
    return $.each(currencies, (function(_this) {
      return function(code, item) {
        return container.append(_this.generateCurrencyItem(code, item.name, item.price));
      };
    })(this));
  };

  ExchangeRates.prototype.generateCurrencyItem = function(code, name, exchengeCost) {
    var item;
    item = $("<li id='currencyItem_" + code + "' class='list-group-item' > <span>1 " + code + "(" + name + ") = " + exchengeCost + " RUB</span> <a href='javascript:void(0);' class='btn btn-sm btn-success pull-right' data-code='" + code + "'> <i class='glyphicon glyphicon-refresh'></i> </a> </li>");
    item.find('a').click(this.updateCurrencyCode);
    return item;
  };

  ExchangeRates.prototype.updateCurrencyCode = function(e) {
    var code;
    code = $(e.currentTarget).data('code');
    $.ajax({
      method: 'post',
      url: UPDATE_URL,
      data: {
        currencyCode: code
      },
      success: function(response) {
        var data, text;
        data = JSON.parse(response);
        text = "1 " + data.code + "(" + data.name + ") = " + data.price + " RUB UU";
        $("#currencyItem_" + code).find('span').text(text);
        return console.log(response);
      },
      error: function(response) {
        return alert(response.responseText);
      }
    });
    return alert('update ' + code);
  };

  ExchangeRates.prototype.initFormAddCurrency = function() {
    return $('#addCurrency').click((function(_this) {
      return function() {
        var currencyCode;
        currencyCode = $('#currencyCode').val();
        if ((currencyCode != null) && currencyCode.trim() !== '') {
          return _this.addCurrency(currencyCode);
        } else {
          return alert('Код валюты не может быть пустым он должен быть формата BRL,EUR ..');
        }
      };
    })(this));
  };

  ExchangeRates.prototype.addCurrency = function(code) {
    return $.ajax({
      method: 'POST',
      url: ADD_CURRENCY_URL,
      data: {
        currencyCode: code
      },
      success: (function(_this) {
        return function(response) {
          var data;
          data = JSON.parse(response);
          $('#currencyList').append(_this.generateCurrencyItem(data.code, data.name, data.price));
          return alert("Валюта " + data.code + " добавлена");
        };
      })(this),
      error: function(response) {
        return alert(response.responseText);
      }
    }).always(function() {
      return console.log('end');
    });
  };

  return ExchangeRates;

})();

$(function() {
  return new ExchangeRates();
});
