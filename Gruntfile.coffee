module.exports = (grunt) ->

  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    coffee:
      glob_to_multiple:
        options:
          bare: true
        expand: true
        flatten: false
        cwd: 'coffee'
        src: ['**/*.coffee']
        dest: 'public/static/js/'
        ext: '.js'
#      compile:
#        options:
#          bare: true
#          join: true
##          sourceMap: true
#        files:
#          'public/js/':['src/main/coffee/**/*.coffee']


    clean: ["public/static/js/**/*"]

    uglify:
      options:
        banner: "public/static/js/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd\") %> */\n"

      build:
        src: "src/<%= pkg.name %>.js"
        dest: "build/<%= pkg.name %>.min.js"


  # Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks "grunt-contrib-uglify"

  grunt.loadNpmTasks 'grunt-contrib-coffee'

  grunt.loadNpmTasks 'grunt-contrib-clean'
  # Default task(s).
  grunt.registerTask "default", ["clean", "coffee"]

  grunt.registerTask "prodaction", ["clean", "coffee"]