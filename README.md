## Курс валют на zend фреймворк
###Установка
создать папку с правами на запись /var/tmp/

###Apache настройки поменять под нуждый
<VirtualHost *:8089>
    # This first-listed virtual host is also the default for *:80
    ServerName tj.anecdot4me.ru
    ServerAlias www.tj.anecdot4me.ru
#    ErrorLog /var/www/tj/www/error_log
    DocumentRoot "/var/www/tj/www"
    <Directory "/var/www/tj/www/">
         Options Indexes FollowSymLinks MultiViews
         AllowOverride All
         Order allow,deny
         Allow from all
#         Deny from all
    </Directory>
</VirtualHost>
### nginx Поменять под свои домены
server {
  listen 80;
  server_name tj.anecdot4me.ru;

  location / {
    proxy_pass http://tj.anecdot4me.ru:8089/public/;
  }
}

### создать БД mysql и выполнить в ней скрипт DB.sql
### Поменять файл настроек application.ini под свои нужды
### Создать файл .htaccess в директории public

RewriteEngine On
# The following rule tells Apache that if the requested filename
# exists, simply serve it.
RewriteCond %{REQUEST_FILENAME} -s [OR]
RewriteCond %{REQUEST_FILENAME} -l [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^.*$ - [NC,L]
# The following rewrites all other queries to index.php. The
# condition ensures that if you are using Apache aliases to do
# mass virtual hosting, the base path will be prepended to
# allow proper resolution of the index.php file; it will work
# in non-aliased environments as well, providing a safe, one-size
# fits all solution.
RewriteCond %{REQUEST_URI}::$1 ^(/.+)(.+)::$
RewriteRule ^(.*)$ - [E=BASE:%1]
RewriteRule ^(.*)$ %{ENV:BASE}index.php [NC,L]
