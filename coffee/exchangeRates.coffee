class ExchangeRates

  ADD_CURRENCY_URL = '/rest/addcurrency'
  LOAD_PRICE_URL = '/rest/load'
  UPDATE_URL = '/rest/update'

  constructor: ->
    @init()


  init: ->
    $.ajax
      method: 'POST'
      url: LOAD_PRICE_URL
      success: (response) =>
        @addCurrencyItems JSON.parse response
        @initFormAddCurrency()
      error: ->
        alert "Сервис временно не доступен"

  addCurrencyItems: (currencies) =>
    container = $ '#currencyList'
    container.empty()
    $.each currencies, (code, item) =>
      container.append @generateCurrencyItem code, item.name, item.price


  generateCurrencyItem: (code, name, exchengeCost) =>
    item = $ "<li id='currencyItem_#{code}' class='list-group-item' >
          <span>1 #{code}(#{name}) = #{exchengeCost} RUB</span>
          <a href='javascript:void(0);' class='btn btn-sm btn-success pull-right' data-code='#{code}'>
            <i class='glyphicon glyphicon-refresh'></i>
          </a>
       </li>"
    item.find('a').click @updateCurrencyCode
    item

  updateCurrencyCode: (e) =>
    code = $(e.currentTarget).data 'code'
    $.ajax
      method: 'post'
      url: UPDATE_URL
      data:
        currencyCode: code
      success: (response) ->
        data = JSON.parse response
        text = "1 #{data.code}(#{data.name}) = #{data.price} RUB"
        $("#currencyItem_#{code}").find('span').text text
        console.log response
      error: (response) ->
        alert response.responseText

  initFormAddCurrency: ->
    $('#addCurrency').click =>
      currencyCode = $('#currencyCode').val()
      if currencyCode? && currencyCode.trim() != ''
        @addCurrency(currencyCode)
      else
        alert 'Код валюты не может быть пустым он должен быть формата BRL,EUR ..'

  addCurrency: (code) =>
    $.ajax
      method: 'POST'
      url: ADD_CURRENCY_URL
      data:
        currencyCode: code
      success: (response) =>
        data = JSON.parse response
        $('#currencyList').append @generateCurrencyItem data.code, data.name, data.price
        alert "Валюта #{data.code} добавлена"
      error: (response) ->
        alert response.responseText
    .always ->
      console.log 'end'

$ ->
  new ExchangeRates()